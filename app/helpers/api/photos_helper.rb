module API::PhotosHelper
  def context_object
    if params[:challenge_id]
      Challenge.find(params[:challenge_id])
    elsif params[:user_id]
      if "me" == params[:user_id].downcase
        current_user
      else
        User.find(params[:user_id])
      end
    else
      nil
    end
  end
end
