class Photo < ActiveRecord::Base

  has_attached_file :image

  validate :submission_limit_not_exceeded

  validates_attachment :image, :content_type => { :content_type => ["image/jpeg", "image/png"] }

  scope :top_by_total_likes, ->(number) { order('cached_votes_up DESC').limit(number) }

  belongs_to :challenge
  belongs_to :user

  acts_as_votable

  private
  def decode_image_data
    data = StringIO.new(Base64.decode64(self.image_data))
    data.class.class_eval {attr_accessor :original_filename, :content_type}
    data.original_filename = self.image_file_name
    data.content_type = self.image_content_type

    self.image = data
  end

  def submission_limit_not_exceeded
    c = self.challenge
    photo_count = c.photos.where(:user_id => self.user_id).count
    if c.submission_limit && photo_count >= c.submission_limit
      errors.add(:submission_limit, "Submission limit of #{c.submission_limit} met for this challenge")
    end
  end
end
