class User < ActiveRecord::Base
  acts_as_voter

  has_many :photos

  validates :uid, uniqueness: true
end
