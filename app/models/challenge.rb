class Challenge < ActiveRecord::Base
  enum challenge_type: { global: 0 , local: 1 }

  before_save :default_type

  reverse_geocoded_by :latitude, :longitude do |obj, results|
    if geo = results.first
      obj.city = geo.city
      obj.state = geo.state
      obj.country = geo.country
    end
  end

  validates_presence_of :title
  #validates_presence_of :description
  #validates_presence_of :latitude
  #validates_presence_of :longitude

  after_validation :reverse_geocode 

  scope :latest, -> { order('created_at DESC').first }
  scope :global, -> { where(:challenge_type => 0) }
  scope :local, -> { where(:challenge_type => 1) }

  has_many :photos

  def address
    [city, state, country].compact.join(', ')
  end

  private
  def default_type
    self.challenge_type ||= "global"
  end
end
