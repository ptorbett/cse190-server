class API::DecodeImage
  attr_accessor :image
  def initialize(data, image_file_name, image_content_type)
    self.image = StringIO.new(Base64.decode64(data))
    self.image.class.class_eval {attr_accessor :original_filename, :content_type}
    self.image.original_filename = image_file_name
    self.image.content_type = image_content_type
  end
end
