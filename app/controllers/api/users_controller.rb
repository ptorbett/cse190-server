class API::UsersController < API::ApplicationController

  def voting_history
    @user = current_user
    @photos = @user.find_voted_items
  end

  def top
    @users = User.joins(:photos).group('users.id').having('SUM(cached_votes_up) > 0').sum('cached_votes_up').sort_by { |k,v| v }.reverse.collect { |x| { User.find(x[0]) => x[1] } }
  end
end
