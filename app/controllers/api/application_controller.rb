class API::ApplicationController < ActionController::API
  include ActionController::ImplicitRender
  include ActionController::Helpers

  rescue_from ActionController::ParameterMissing, with: :parameter_missing
  rescue_from ActiveRecord::RecordNotFound, with: :record_not_found
  rescue_from FbGraph::InvalidToken, with: :authentication_failed

  private

  def current_user
    unless @current_user
      params.require(:fb_access_token)
      user = FbGraph::User.me(params[:fb_access_token]).fetch
      u = User.where(:uid => user.raw_attributes[:id]).first
      unless u
        u = User.new(:uid => user.raw_attributes[:id], :name => user.name)
        unless u.save
          raise Error.new("User was not saved")
        end
      end
      @current_user = u
    end
    @current_user
  end

  def invalid_parameter(error)
    render json: { :error => error.message}, status: :unprocessable_entity
  end

  def parameter_missing(error)
    render json: { :error => error.message}, status: :unprocessable_entity
  end

  def record_not_found(error)
    render json: {:error => error.message}, status: :not_found
  end

  def authentication_failed(error)
    render json: {:error => error.message}, status: :unauthorized
  end
end
