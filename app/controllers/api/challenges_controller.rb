class API::ChallengesController < API::ApplicationController

  def index
    @challenges = Challenge.all.global
  end

  def show
    @challenge = Challenge.find(params[:id])
  end

  def random
    @challenge = Challenge.all.global.sample
  end

  def create
    @challenge = Challenge.new(challenge_params)
    @success = @challenge.save
    if @success
      render status: :created
    else
      if @challenge.invalid?
        render status: :unprocessable_entity
      else
        render status: :internal_server_error
      end
    end
  end

  def latest
    @challenge = Challenge.global.latest
  end

  def nearby
    distance = params[:distance]
    distance ||= 20
    @city = ""
    if params[:challenge_id]
      challenge = Challenge.find(params[:challenge_id])
      @challenges = Challenge.near(challenge, distance)
      if @challenges.first
        geo = @challenges.first
        @city = geo.city
        @state = geo.state
        @country = geo.country
      end
    elsif params[:latitude] && params[:longitude]
      g = Geocoder.search("#{params[:latitude]}, #{params[:longitude]}")
      if g.count != 0 && g.first
        geo = g.first
        @city = geo.city
        @state = geo.state
        @country = geo.country
      end
      @challenges = Challenge.near([params[:latitude], params[:longitude]], distance)
    elsif params[:city] && params[:state] && params[:country]
      @city = params[:city]
      @state = params[:state]
      @country = params[:country]
      query_s = "#{@city}, #{@state} #{@country}"
      @challenges = Challenge.near(query_s, distance)
    else
      parameter_missing(Exception.new("Missing parameter. Must specify challenge_id OR latitude and longitude OR city/state/country"))
    end
  end

  private
  def challenge_params
    params.require(:challenge).permit(
      :title, :description, :latitude, :longitude, :challenge_type,
      :submission_limit
    )
  end
end
