class API::PhotosController < API::ApplicationController
  include API::PhotosHelper

  def index
    @photos = context_object.photos
  end

  def create
    @photo = context_object.photos.build(photo_params)
    @photo.user = current_user
    @success = @photo.save
    if @success
      @photo.liked_by current_user
      render
    else
      if @photo.invalid?
        render status: :unprocessable_entity
      else
        render status: :internal_server_error
      end
    end
  end

  def vote
    @photo = Photo.find(params[:id])
    vote = params.require(:vote)[:vote_result]
    if "up" == vote.downcase
      @photo.liked_by current_user
    elsif "down" == vote.downcase
      @photo.disliked_by current_user
    else
      invalid_parameter(Error.new("votes must be 'up' or 'down'"))
    end
    @vote = current_user.voted_as_when_voted_for @photo
  end

  def random
    photos = context_object.photos.select { |p| !current_user.voted_for? p }
    @photo = photos.sample
  end

  def top
    number = params[:number]
    number ||= 10
    if context_object
      @photos = context_object.photos.top_by_total_likes(number)
    else
      @photos = Photo.top_by_total_likes(number)
    end
  end

  private

  def photo_params
    temp = params.require(:photo).permit(
      :caption, :image_file_name, :image_content_type, :image_data
    )
    temp.require(:image_data)
    temp.require(:image_content_type)
    temp.require(:image_file_name)
    image = API::DecodeImage.new(temp[:image_data], temp[:image_file_name], temp[:image_content_type]).image
    params.require(:photo).permit(
      :caption
    ).merge(image: image)
  end
end
