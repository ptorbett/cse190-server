json.photos @photos do |photo|
  json.photo do
    json.partial! 'api/photos/photo', photo: photo
  end
  json.challenge do
    json.partial! 'api/challenges/challenge', challenge: photo.challenge
  end
  json.vote (@user.voted_as_when_voted_for photo) ? "likes" : "dislikes"
end
