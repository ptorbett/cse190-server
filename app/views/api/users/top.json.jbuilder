json.users @users do |hash|
  json.user do
    json.name hash.keys.first.name
    json.likes hash.values.first
  end
end
