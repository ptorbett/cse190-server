json.city @city
json.state @state
json.country @country

json.challenges @challenges do |challenge|
  json.challenge do
    json.partial! 'challenge', challenge: challenge
  end
end
