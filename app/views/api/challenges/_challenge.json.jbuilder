json.id challenge.id
json.title challenge.title
json.description challenge.description
json.created_at challenge.created_at
json.updated_at challenge.updated_at
json.city challenge.city
json.state challenge.state
json.country challenge.country
