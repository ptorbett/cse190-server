json.success @success
json.challenge do
  json.partial! 'challenge', challenge: @challenge
end
