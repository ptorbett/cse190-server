json.challenges @challenges do |challenge|
  json.challenge do
    json.partial! 'challenge', challenge: challenge
  end
end
