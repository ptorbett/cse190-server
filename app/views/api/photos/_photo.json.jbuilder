json.id photo.id
json.description photo.description
json.challenge_id photo.challenge_id
json.image URI.join(request.url, photo.image.url).to_s
