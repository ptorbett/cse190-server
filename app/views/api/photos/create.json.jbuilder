json.success @success
unless @success
  json.errors @photo.errors.full_messages
end

json.photo do
  json.partial! 'photo', photo: @photo
end
