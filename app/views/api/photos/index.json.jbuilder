json.photos @photos do |photo|
  json.photo do
    json.partial! 'photo', photo: photo
  end
  json.challenge do
    json.partial! 'api/challenges/challenge', challenge: photo.challenge
  end
  json.votes do
    json.total photo.votes.size
    json.likes photo.likes.size
    json.dislikes photo.dislikes.size
  end
end
