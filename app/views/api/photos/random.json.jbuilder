if @photo
  json.partial! 'photo', photo: @photo
end
