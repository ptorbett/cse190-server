Rails.application.routes.draw do

  namespace :api, :defaults => {:format => :json} do
    resources :photos do
      collection do
        get 'top'
      end
    end

    resources :challenges do
      resources :photos, :shallow => true do
        member do
          post 'vote'
        end
        collection do
          get 'top'
        end
        collection do
          get 'random'
        end
      end
      collection do
        get 'latest'
        get 'random'
        get 'nearby'
      end
    end

    resources :users do
      resources :photos, :shallow => true do
        member do
          post 'vote'
        end
        collection do
          get 'top'
        end
      end
      member do
        get 'voting_history'
      end
      collection do
        get 'nearby'
        get 'top'
      end
    end
  end
end
