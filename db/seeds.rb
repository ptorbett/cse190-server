# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or create!d alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create!([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create!(name: 'Emanuel', city: cities.first)

u1 = User.create!(
  {
    uid: "531221649",
    name: "Jon Lin"
  }
)

u2 = User.create!(
  {
    uid: "662133084",
    name: "Patrick Torbett"
  }
)

c = Challenge.create!(
  {
    title: "Take a picture of your pet.",
    description: ""
  }
)
c.photos.create!(
  {
    description: "Bagel the corgi with a unicorn",
    image: File.open(File.join(Rails.root, '/db/seeds/1_pet.jpg')),
    user: u1
  }
)


c = Challenge.create!(
  {
    title: "Take a picture with a pet that resembles a celebrity.",
    description: ""
  }
)
c.photos.create!(
  {
    description: "Ron Perlman",
    image: File.open(File.join(Rails.root, '/db/seeds/2_1_pet_celebrity.jpg')),
    user: u1
  }
)
c.photos.create!(
  {
    description: "Whoopi Goldberg",
    image: File.open(File.join(Rails.root, '/db/seeds/2_2_pet_celebrity.jpg')),
    user: u1
  }
)
c.photos.create!(
  {
    description: "Paris Hilton",
    image: File.open(File.join(Rails.root, '/db/seeds/2_3_pet_celebrity.jpg')),
    user: u1
  }
)
c.photos.create!(
  {
    description: "Rick James",
    image: File.open(File.join(Rails.root, '/db/seeds/2_4_pet_celebrity.jpg')),
    user: u2
  }
)
c.photos.create!(
  {
    description: "J.J. Abrams",
    image: File.open(File.join(Rails.root, '/db/seeds/2_5_pet_celebrity.jpg')),
    user: u2
  }
)

c = Challenge.create!(
  {
    title: "Take a picture of a human tower.",
    description: ""
  }
)
c.photos.create!(
  {
    description: "Tower 1",
    image: File.open(File.join(Rails.root, '/db/seeds/3_1_human_tower.jpg')),
    user: u1
  }
)
c.photos.build(
  {
    description: "Tower 2",
    image: File.open(File.join(Rails.root, '/db/seeds/3_2_human_tower.jpg')),
    user: u2
  }
)

c = Challenge.create!(
  {
    title: "Take a picture of yourself in a halloween costume.",
    description: ""
  }
)
c.photos.create!(
  {
    description: "Ipods + Earbuds",
    image: File.open(File.join(Rails.root, '/db/seeds/4_1_halloween_costume.jpg')),
    user: u1
  }
)
c.photos.create!(
  {
    description: "Geth Costume",
    image: File.open(File.join(Rails.root, '/db/seeds/4_2_halloween_costume.jpg')),
    user: u2
  }
)

c = Challenge.create!(
  {
    title: "Take a picture of a badass grandma!",
    description: ""
  }
)
c.photos.create!(
  {
    description: "Work it out",
    image: File.open(File.join(Rails.root, '/db/seeds/5_1_badass_grandma.jpg')),
    user: u1
  }
)
c.photos.create!(
  {
    description: "I've got a hip hip",
    image: File.open(File.join(Rails.root, '/db/seeds/5_2_badass_grandma.jpg')),
    user: u2
  }
)
c.photos.create!(
  {
    description: "Skater grandma",
    image: File.open(File.join(Rails.root, '/db/seeds/5_3_badass_grandma.jpg')),
    user: u1
  }
)

c = Challenge.create!(
  {
    title: "Take a picture of a baby's funny facial expression",
    description: ""
  }
)
c.photos.create!(
  {
    description: "What are you looking at?",
    image: File.open(File.join(Rails.root, '/db/seeds/6_1_baby_facial_expression.jpg')),
    user: u2
  }
)
c.photos.create!(
  {
    description: "Selfie time!",
    image: File.open(File.join(Rails.root, '/db/seeds/6_2_baby_facial_expression.jpg')),
    user: u1
  }
)
c.photos.create!(
  {
    description: "They cancelled Community?!?!?!?",
    image: File.open(File.join(Rails.root, '/db/seeds/6_3_baby_facial_expression.jpg')),
    user: u1
  }
)
c.photos.create!(
  {
    description: "Can't a baby get some sleep around here?",
    image: File.open(File.join(Rails.root, '/db/seeds/6_4_baby_facial_expression.jpg')),
    user: u1
  }
)

c = Challenge.create!(
  {
    title: "Take a picture of a culinary masterpiece",
    description: ""
  }
)
c.photos.create!(
  {
    description: "Culinary 1",
    image: File.open(File.join(Rails.root, '/db/seeds/7_1_culinary_masterpiece.jpg')),
    user: u1
  }
)
c.photos.create!(
  {
    description: "Culinary 2",
    image: File.open(File.join(Rails.root, '/db/seeds/7_2_culinary_masterpiece.jpg')),
    user: u1
  }
)
c.photos.create!(
  {
    description: "Culinary 3",
    image: File.open(File.join(Rails.root, '/db/seeds/7_3_culinary_masterpiece.jpg')),
    user: u1
  }
)
c.photos.create!(
  {
    description: "Culinary 4",
    image: File.open(File.join(Rails.root, '/db/seeds/7_4_culinary_masterpiece.jpg')),
    user: u2
  }
)

c = Challenge.create!(
  {
    title: "Take a picture of your most embarassing school photo",
    description: ""
  }
)
c.photos.create!(
  {
    description: "Embarassing 1",
    image: File.open(File.join(Rails.root, '/db/seeds/8_1_embarassing_school_photo.jpg')),
    user: u1
  }
)
c.photos.create!(
  {
    description: "Embarassing 2",
    image: File.open(File.join(Rails.root, '/db/seeds/8_2_embarassing_school_photo.jpg')),
    user: u1
  }
)
c.photos.create!(
  {
    description: "Embarassing 3",
    image: File.open(File.join(Rails.root, '/db/seeds/8_3_embarassing_school_photo.jpg')),
    user: u1
  }
)
c.photos.create!(
  {
    description: "Embarassing 4",
    image: File.open(File.join(Rails.root, '/db/seeds/8_4_embarassing_school_photo.jpg')),
    user: u2
  }
)
c.photos.create!(
  {
    description: "Embarassing 5",
    image: File.open(File.join(Rails.root, '/db/seeds/8_5_embarassing_school_photo.jpg')),
    user: u2
  }
)

c = Challenge.create!(
  {
    title: "Take a picture of something upside-down",
    description: ""
  }
)
c.photos.create!(
  {
    description: "Upside Down 1",
    image: File.open(File.join(Rails.root, '/db/seeds/9_1_upside_down.jpg')),
    user: u1
  }
)
c.photos.create!(
  {
    description: "Upside Down 2",
    image: File.open(File.join(Rails.root, '/db/seeds/9_2_upside_down.jpg')),
    user: u1
  }
)
c.photos.create!(
  {
    description: "Upside Down 3",
    image: File.open(File.join(Rails.root, '/db/seeds/9_3_upside_down.jpg')),
    user: u1
  }
)
c.photos.create!(
  {
    description: "Upside Down 4",
    image: File.open(File.join(Rails.root, '/db/seeds/9_4_upside_down.jpg')),
    user: u2
  }
)
c.photos.create!(
  {
    description: "Upside Down 5",
    image: File.open(File.join(Rails.root, '/db/seeds/9_5_upside_down.jpg')),
    user: u2
  }
)

c = Challenge.create!(
  {
    title: "Take a picture of the Blood Moon",
    description: ""
  }
)
c.photos.create!(
  {
    description: "Blood Moon 1",
    image: File.open(File.join(Rails.root, '/db/seeds/10_1_blood_moon.jpg')),
    user: u1
  }
)
c.photos.create!(
  {
    description: "Blood Moon 2",
    image: File.open(File.join(Rails.root, '/db/seeds/10_2_blood_moon.jpg')),
    user: u1
  }
)
c.photos.create!(
  {
    description: "Blood Moon 3",
    image: File.open(File.join(Rails.root, '/db/seeds/10_3_blood_moon.jpg')),
    user: u2
  }
)
c.photos.create!(
  {
    description: "Blood Moon 4",
    image: File.open(File.join(Rails.root, '/db/seeds/10_4_blood_moon.jpg')),
    user: u2
  }
)

c = Challenge.create!(
  {
    title: "Take a picture of your favorite dessert",
    description: ""
  }
)
c.photos.create!(
  {
    description: "Favorite Dessert 1",
    image: File.open(File.join(Rails.root, '/db/seeds/11_1_favorite_dessert.jpg')),
    user: u1
  }
)
c.photos.create!(
  {
    description: "Favorite Dessert 2",
    image: File.open(File.join(Rails.root, '/db/seeds/11_2_favorite_dessert.jpg')),
    user: u1
  }
)
c.photos.create!(
  {
    description: "Favorite Dessert 3",
    image: File.open(File.join(Rails.root, '/db/seeds/11_3_favorite_dessert.jpg')),
    user: u2
  }
)

c = Challenge.create!(
  {
    title: "Take a picture of a weird car",
    description: ""
  }
)
c.photos.create!(
  {
    description: "Weird Car 1",
    image: File.open(File.join(Rails.root, '/db/seeds/12_1_weird_car.jpg')),
    user: u1
  }
)
c.photos.create!(
  {
    description: "Weird Car 2",
    image: File.open(File.join(Rails.root, '/db/seeds/12_2_weird_car.jpg')),
    user: u1
  }
)
c.photos.create!(
  {
    description: "Weird Car 3",
    image: File.open(File.join(Rails.root, '/db/seeds/12_3_weird_car.jpg')),
    user: u2
  }
)
c.photos.create!(
  {
    description: "Weird Car 4",
    image: File.open(File.join(Rails.root, '/db/seeds/12_4_weird_car.jpg')),
    user: u1
  }
)
c.photos.create!(
  {
    description: "Weird Car 5",
    image: File.open(File.join(Rails.root, '/db/seeds/12_5_weird_car.jpg')),
    user: u1
  }
)
c.photos.create!(
  {
    description: "Weird Car 6",
    image: File.open(File.join(Rails.root, '/db/seeds/12_6_weird_car.jpg')),
    user: u2
  }
)
c.photos.create!(
  {
    description: "Weird Car 7",
    image: File.open(File.join(Rails.root, '/db/seeds/12_7_weird_car.jpg')),
    user: u1
  }
)
c.photos.create!(
  {
    description: "Weird Car 8",
    image: File.open(File.join(Rails.root, '/db/seeds/12_8_weird_car.jpg')),
    user: u1
  }
)
c.photos.create!(
  {
    description: "Weird Car 9",
    image: File.open(File.join(Rails.root, '/db/seeds/12_9_weird_car.jpg')),
    user: u2
  }
)
c.photos.create!(
  {
    description: "Weird Car 10",
    image: File.open(File.join(Rails.root, '/db/seeds/12_10_weird_car.jpg')),
    user: u1
  }
)

c = Challenge.create!(
  {
    title: "Take a picture of an unconventional hairstyle",
    description: ""
  }
)
c.photos.create!(
  {
    description: "Unconventional Hairstyle 1",
    image: File.open(File.join(Rails.root, '/db/seeds/13_1_hairstyle.jpg')),
    user: u1
  }
)
c.photos.create!(
  {
    description: "Unconventional Hairstyle 2",
    image: File.open(File.join(Rails.root, '/db/seeds/13_2_hairstyle.jpg')),
    user: u1
  }
)
c.photos.create!(
  {
    description: "Unconventional Hairstyle 3",
    image: File.open(File.join(Rails.root, '/db/seeds/13_3_hairstyle.jpg')),
    user: u2
  }
)
c.photos.create!(
  {
    description: "Unconventional Hairstyle 4",
    image: File.open(File.join(Rails.root, '/db/seeds/13_4_hairstyle.jpg')),
    user: u2
  }
)

c = Challenge.create!(
  {
    title: "Take a picture of half your face with make up, half without",
    description: ""
  }
)
c.photos.create!(
  {
    description: "Half-face Makeup 1",
    image: File.open(File.join(Rails.root, '/db/seeds/14_1_makeup.jpg')),
    user: u1
  }
)
c.photos.create!(
  {
    description: "Half-face Makeup 2",
    image: File.open(File.join(Rails.root, '/db/seeds/14_2_makeup.jpg')),
    user: u1
  }
)
c.photos.create!(
  {
    description: "Half-face Makeup 3",
    image: File.open(File.join(Rails.root, '/db/seeds/14_3_makeup.jpg')),
    user: u2
  }
)

c = Challenge.create!(
  {
    title: "Take a picture of what you do for a living",
    description: ""
  }
)
c.photos.create!(
  {
    description: "Work 1",
    image: File.open(File.join(Rails.root, '/db/seeds/15_1_work.jpg')),
    user: u1
  }
)
c.photos.create!(
  {
    description: "Work 2",
    image: File.open(File.join(Rails.root, '/db/seeds/15_2_work.jpg')),
    user: u1
  }
)
c.photos.create!(
  {
    description: "Work 3",
    image: File.open(File.join(Rails.root, '/db/seeds/15_3_work.jpg')),
    user: u1
  }
)
c.photos.create!(
  {
    description: "Work 4",
    image: File.open(File.join(Rails.root, '/db/seeds/15_4_work.jpg')),
    user: u2
  }
)
c.photos.create!(
  {
    description: "Work 5",
    image: File.open(File.join(Rails.root, '/db/seeds/15_5_work.jpg')),
    user: u2
  }
)
