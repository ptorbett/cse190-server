class AddAddressInformationToChallenges < ActiveRecord::Migration
  def change
    add_column :challenges, :city, :string
    add_column :challenges, :state, :string
    add_column :challenges, :country, :string
  end
end
