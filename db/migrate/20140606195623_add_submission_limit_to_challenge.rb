class AddSubmissionLimitToChallenge < ActiveRecord::Migration
  def change
    add_column :challenges, :submission_limit, :integer
  end
end
