class AddChallengeIdToPhotos < ActiveRecord::Migration
  def change
    add_column :photos, :challenge_id, :integer
  end
end
