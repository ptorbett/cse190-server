class AddChallengeTypeToChallenge < ActiveRecord::Migration
  def change
    add_column :challenges, :challenge_type, :integer
  end
end
