FactoryGirl.define do
  factory :challenge do
    title 'Create test data'
    description 'Come up with interesting test data placeholders'
    latitude '32.877491'
    longitude '-117.235276'
  end
end
