require 'spec_helper'

describe 'Challenges API' do
  let(:accept_json) { { 'Accept' => 'application/json' } }
  let(:content_type_json) { { 'Content-Type' => 'application/json' } }
  let(:accept_and_return_json) { accept_json.merge content_type_json }

  describe 'GET /api/challenges/:id' do
    let(:challenge) { FactoryGirl.create(:challenge, title: 'Break the API Part 3') }

    it 'returns the requested challenge' do
      get "/api/challenges/#{challenge.id}", {}, accept_json

      expect(response.status).to be 200

      body = JSON.parse(response.body)
      expect(body['challenge']['title']).to eq "Break the API Part 3"
    end
  end

  describe 'GET /api/challenges/latest' do
    it 'returns the most recent challenge' do
      FactoryGirl.create :challenge, title: 'Break the API'
      FactoryGirl.create :challenge, title: "Don't break the API"

      get '/api/challenges/latest', {}, accept_json
      expect(response.status).to eq 200

      body = JSON.parse(response.body)
      challenge_title = body['challenge']['title']

      expect(challenge_title).to eq "Don't break the API"
    end
  end

  describe 'POST /api/challenges' do
    it 'creates a challenge' do
      challenge_params = {
        'challenge' => {
          'title' => "Don't break the API Part 2: Return of the living dead",
          'description' => 'Nothing has broken this badly since Mr. White',
          'latitude' => '34.988889',
          'longitude' => '-106.614444'
        }
      }.to_json

      request_headers = accept_and_return_json

      post '/api/challenges/', challenge_params, request_headers

      expect(response.status).to eq 201
      expect(Challenge.first.title).to eq "Don't break the API Part 2: Return of the living dead"
    end
  end
end
